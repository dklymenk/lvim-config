local DATA_PATH = vim.fn.stdpath "data"

-- general
lvim.log.level = "warn"
lvim.format_on_save = false
lvim.lint_on_save = true
lvim.colorscheme = "onedarker"
lvim.transparent_window = true
vim.opt.wrap = true
vim.opt.relativenumber = true

-- keymappings
-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader = "space"
lvim.keys.normal_mode["X"] = ":BufferKill<CR>"
-- lsp shortcuts
lvim.keys.normal_mode["gd"] = "<cmd>lua vim.lsp.buf.definition()<CR>"
lvim.keys.normal_mode["gD"] = "<cmd>lua vim.lsp.buf.declaration()<CR>"
lvim.keys.normal_mode["gr"] = "<cmd>lua vim.lsp.buf.references()<CR>"
lvim.keys.normal_mode["gi"] = "<cmd>lua vim.lsp.buf.implementation()<CR>"
lvim.keys.normal_mode["gl"] =
  "<cmd>lua vim.lsp.diagnostic.show_line_diagnostics({ show_header = false, border = 'single' })<CR>"
lvim.keys.normal_mode["gs"] = "<cmd>lua vim.lsp.buf.signature_help()<CR>"
lvim.keys.normal_mode["gp"] = "<cmd>lua require'lsp.peek'.Peek('definition')<CR>"
lvim.keys.normal_mode["K"] = "<cmd>lua vim.lsp.buf.hover()<CR>"
-- skip typescript hints when cycling diagnostics
lvim.keys.normal_mode["<C-p>"] =
  '<cmd>lua vim.diagnostic.goto_prev({popup_opts = {border = lvim.lsp.popup_border}, severity_limit = vim.bo.filetype == \'typescriptreact\' and "Information" or "Hint"})<CR>'
lvim.keys.normal_mode["<C-n>"] =
  '<cmd>lua vim.diagnostic.goto_next({popup_opts = {border = lvim.lsp.popup_border}, severity_limit = vim.bo.filetype == \'typescriptreact\' and "Information" or "Hint"})<CR>'
-- cycling through copilot suggestions
vim.api.nvim_set_keymap("i", "<C-n>", "<Plug>(copilot-next)", { noremap = true, silent = true })
vim.api.nvim_set_keymap("i", "<C-p>", "<Plug>(copilot-prev)", { noremap = true, silent = true })

-- Change Telescope navigation to use j and k for navigation and n and p for history in both input and normal mode.
-- we use protected-mode (pcall) just in case the plugin wasn't loaded yet.
local _, actions = pcall(require, "telescope.actions")
lvim.builtin.telescope.defaults.mappings = {
  -- for input mode
  i = {
    ["<C-j>"] = actions.move_selection_next,
    ["<C-k>"] = actions.move_selection_previous,
    ["<C-n>"] = actions.cycle_history_next,
    ["<C-p>"] = actions.cycle_history_prev,
  },
  -- for normal mode
  n = {
    ["<C-j>"] = actions.move_selection_next,
    ["<C-k>"] = actions.move_selection_previous,
  },
}
lvim.builtin.telescope.defaults.file_ignore_patterns = {
  "vendor/*",
  "%.lock",
  "__pycache__/*",
  "%.sqlite3",
  "%.ipynb",
  "node_modules/*",
  "%.jpg",
  "%.jpeg",
  "%.png",
  "%.svg",
  "%.otf",
  "%.ttf",
  ".git/",
  "%.webp",
  ".dart_tool/",
  ".github/",
  ".gradle/",
  ".idea/",
  ".settings/",
  ".vscode/",
  "__pycache__/",
  "build/",
  "env/",
  "gradle/",
  "node_modules/",
  "target/",
  "%.pdb",
  "%.dll",
  "%.class",
  "%.exe",
  "%.cache",
  "%.ico",
  "%.pdf",
  "%.dylib",
  "%.jar",
  "%.docx",
  "%.met",
  "smalljre_*/*",
  ".vale/",
  "%.burp",
  "%.mp4",
  "%.mkv",
  "%.rar",
  "%.zip",
  "%.7z",
  "%.tar",
  "%.bz2",
  "%.epub",
  "%.flac",
  "%.tar.gz",
  "%.min.js",
  "assets/*.js",
  "assets/*.css",
  "dist/*.js",
  "dist/*.css",
  "wp-includes/*",
  "wp-admin/*",
}
lvim.builtin.telescope.defaults.preview = { treesitter = false }

-- Use which-key to add extra bindings with the leader-key prefix
lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }
lvim.builtin.which_key.mappings["t"] = {
  name = "+Trouble",
  r = { "<cmd>Trouble lsp_references<cr>", "References" },
  f = { "<cmd>Trouble lsp_definitions<cr>", "Definitions" },
  d = { "<cmd>Trouble lsp_document_diagnostics<cr>", "Diagnostics" },
  q = { "<cmd>Trouble quickfix<cr>", "QuickFix" },
  l = { "<cmd>Trouble loclist<cr>", "LocationList" },
  w = { "<cmd>Trouble lsp_workspace_diagnostics<cr>", "Diagnostics" },
}
lvim.builtin.which_key.mappings["r"] = { "<cmd>RnvimrToggle<cr>", "Ranger" }

-- TODO: User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.treesitter.ensure_installed = {
  "bash",
  "comment",
  "css",
  "dockerfile",
  "graphql",
  "html",
  "javascript",
  "json",
  "lua",
  "php",
  "prisma",
  "tsx",
  "typescript",
  "yaml",
}
lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enabled = true
-- lvim.builtin.cmp = false
-- lvim.builtin.cmp.formatting.source_names["copilot"] = "(Copilot)"
-- lvim.builtin.cmp.formatting.kind_icons["Copilot"] = " "
-- table.insert(lvim.builtin.cmp.sources, 1, { name = "copilot" })

-- generic LSP settings

-- ---@usage disable automatic installation of servers
-- lvim.lsp.automatic_servers_installation = false

-- ---@usage Select which servers should be configured manually. Requires `:LvimCacheRest` to take effect.
-- See the full default list `:lua print(vim.inspect(lvim.lsp.override))`
vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "tsserver", "intelephense", "texlab" })

-- ---@usage setup a server -- see: https://www.lunarvim.org/languages/#overriding-the-default-configuration
require("lspconfig").tailwindcss.setup {
  settings = {
    tailwindCSS = {
      classAttributes = { "class", "className", "classList" },
    },
  },
}
require("lvim.lsp.manager").setup("tsserver", {
  root_dir = require("lspconfig").util.root_pattern("Makefile", ".git", "node_modules"),
  -- handlers = { ["textDocument/publishDiagnostics"] = function() end },
})
require("lvim.lsp.manager").setup("texlab", {
  settings = {
    texlab = {
      chktex = {
        onEdit = true,
        onOpenAndSave = true,
      },
      build = {
        args = { "%f" },
        executable = "pdflatex",
        onSave = true,
      },
    },
  },
})
require("lvim.lsp.manager").setup("intelephense", {
  settings = {
    intelephense = {
      files = {
        associations = {
          "*.php",
          "*.phtml",
          "*.inc",
          "*.module",
          "*.install",
          "*.theme",
          ".engine",
          ".profile",
          ".info",
          ".test",
        },
      },
    },
  },
})
require("lspconfig")["phpactor"].setup {
  handlers = {
    ["textDocument/hover"] = function() end,
  },
}

-- you can set a custom on_attach function that will be used for all the language servers
-- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
-- lvim.lsp.on_attach_callback = function(client, bufnr)
--   local function buf_set_option(...)
--     vim.api.nvim_buf_set_option(bufnr, ...)
--   end
--   --Enable completion triggered by <c-x><c-o>
--   buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
-- end

-- add node_modules to null_ls root pattern
lvim.lsp.null_ls.setup = {
  root_dir = require("lspconfig").util.root_pattern("Makefile", ".git", "node_modules"),
}

-- -- set a formatter, this will override the language server formatting capabilities (if it exists)
local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
  {
    exe = "eslint_d",
    filetypes = { "javascript", "javascriptreact", "typescript", "typescriptreact" },
  },
  -- {
  --   exe = "prettier",
  --   filetypes = { "javascript", "javascriptreact", "typescript", "typescriptreact" },
  -- },
  {
    exe = "stylua",
    filetypes = { "lua" },
  },
}

-- -- set additional linters
local linters = require "lvim.lsp.null-ls.linters"
linters.setup {
  {
    exe = "eslint_d",
    filetypes = { "javascript", "javascriptreact", "typescript", "typescriptreact" },
  },
}

-- Additional Plugins
lvim.plugins = {
  { "kevinhwang91/rnvimr" },
  { "iamcco/markdown-preview.nvim", run = "cd app && yarn install" },
  { "mattn/emmet-vim" },
  { "tzachar/cmp-tabnine", run = "./install.sh", requires = "hrsh7th/nvim-cmp" },
  {
    "norcalli/nvim-colorizer.lua",
    config = function()
      require("colorizer").setup({ "css", "scss", "html", "javascript" }, {
        RGB = true, -- #RGB hex codes
        RRGGBB = true, -- #RRGGBB hex codes
        RRGGBBAA = true, -- #RRGGBBAA hex codes
        rgb_fn = true, -- CSS rgb() and rgba() functions
        hsl_fn = true, -- CSS hsl() and hsla() functions
        css = true, -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
        css_fn = true, -- Enable all CSS *functions*: rgb_fn, hsl_fn
      })
    end,
  },
  { "ruanyl/vim-gh-line" },
  {
    "folke/trouble.nvim",
    cmd = "TroubleToggle",
  },
  { "nelsyeung/twig.vim" },
  {
    "github/copilot.vim",
    config = function()
      -- copilot assume mapped
      vim.g.copilot_assume_mapped = true
      -- vim.g.copilot_no_tab_map = true
      vim.g.copilot_filetypes = { markdown = true }
    end,
  },
  -- {
  --   "zbirenbaum/copilot.lua",
  --   event = { "VimEnter" },
  --   config = function()
  --     vim.defer_fn(function()
  --       require("copilot").setup {
  --         plugin_manager_path = get_runtime_dir() .. "/site/pack/packer",
  --       }
  --     end, 100)
  --   end,
  -- },
  -- {
  --   "zbirenbaum/copilot-cmp",
  --   after = { "copilot.lua", "nvim-cmp" },
  -- },
}

lvim.autocommands = {
  -- select indentation by language
  -- { "FileType", { pattern = "php", command = "setlocal ts=2 sw=2 sts=2" } },
  { "FileType", { pattern = "php", command = "setlocal ts=4 sw=4 sts=4" } },
  { "FileType", { pattern = "javascript", command = "setlocal ts=2 sw=2 sts=2" } },
  -- { "FileType", { pattern = "javascript", command = "setlocal ts=4 sw=4 sts=4" } },
  { "BufRead,BufNewFile", { pattern = "*.theme", command = "set filetype=php" } },
}

-- Load custom snippets
require("luasnip/loaders/from_vscode").lazy_load { paths = "~/.config/lvim/snippets" }

local tabnine = require "cmp_tabnine.config"
tabnine:setup {
  max_lines = 1000,
  max_num_results = 20,
  sort = true,
  run_on_every_keystroke = true,
  snippet_placeholder = "..",
  ignored_file_types = { -- default is not to ignore
    -- uncomment to ignore in lua:
    -- lua = true
  },
  show_prediction_strength = false,
}
